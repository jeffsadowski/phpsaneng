<?php
/*

  http://raspberrypi/scanimage.php?options=-h+-d=brother2:bus1;dev1
  I use urlencode for options

  commandline
  php scanimage.php -h -d "brother2:bus1;dev1"

  above should start another php process in the background via curl

*/

ignore_user_abort(true);
set_time_limit(0);

if(sizeof($argv)>0)
{$myargv=$argv;
 array_shift($myargv);
 if(sizeof($argv)>1)
 {$_REQUEST['options']="'".implode("' '",$myargv)."'";
 }
 $usertime='scanimage-'.posix_geteuid().'-'.time();
 $_REQUEST['tmpdir']='/tmp/'. $usertime;
 $_REQUEST['background']=true;
 if(!mkdir($_REQUEST['tmpdir']))
 {echo "Can not create directory '".$_REQUEST['tmpdir']."'\n";
  exit();
}}

function clean($string)
{$string = str_replace('`', '', $string);
 $string = str_replace('$', '', $string);
 $string = str_replace('(', '', $string);
 $string = str_replace(')', '', $string);
 $string = str_replace('*', '', $string);
 $string = str_replace('{', '', $string);
 $string = str_replace('}', '', $string);
 $string = str_replace('[', '', $string);
 $string = str_replace(']', '', $string);
 return $string;
}

if(isset($_REQUEST['options']))
{$_REQUEST['options']=clean($_REQUEST['options']);
}

if(isset($_REQUEST['background']))
{//run only output to files
 $ran_file=tname('ran.txt');
 $stdout_file=tname('stdout.txt');
 $stderr_file=tname('stderr.txt');
 $running_file=tname('running.txt');
 $term_file=tname('term.txt');
 // fix escape vector
 if(!isset($_REQUEST['options'])){$_REQUEST['options']="-L";}
 $options=explode("'",$_REQUEST['options']);
 $_REQUEST['options']="";
 foreach($options as $option)
 {if(!preg_match("/^\s*$/",$option))
  {$_REQUEST['options'].=" '".$option."'";
 }}
 echo $_REQUEST['options']."\n";
}
else
{$usertime='scanimage-'.posix_geteuid().'-'.time();
 if(!isset($_REQUEST['tmpdir']))
 {$_REQUEST['tmpdir']=DIRECTORY_SEPARATOR . trim(ini_get('session.save_path'), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $usertime;
  if(!mkdir($_REQUEST['tmpdir']))
  {$_REQUEST['tmpdir']=DIRECTORY_SEPARATOR . trim(sys_get_temp_dir(), DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . $usertime;
   if(!mkdir($_REQUEST['tmpdir']))
   {echo "ERROR Could not create a temp file\n";
    exit();
  }}
  echo $_REQUEST['tmpdir'];
 }

 $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
 $join='?';
 if(preg_match("/[?]/",$actual_link))
 {$join='&';
 }
 $url=$actual_link.$join.'background=true&tmpdir='.urlencode($_REQUEST['tmpdir']);
 $ch = curl_init();
 curl_setopt($ch, CURLOPT_URL, $url);
 curl_setopt($ch, CURLOPT_TIMEOUT_MS, 1);
 curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
 curl_exec($ch);
 curl_close($ch);
 exit();
}

//example
//scanimage('-h -d "brother2:bus1;dev1"');
function tname($name)
{return $_REQUEST['tmpdir'] . DIRECTORY_SEPARATOR . ltrim($name, DIRECTORY_SEPARATOR);
}

function scanimage($options)
{global $stdout_file,$stderr_file,$running_file,$ran_file,$term_file;
 file_put_contents($running_file,"true\n");
 $stdin=array("pipe","r");
 $stdout=array("pipe","w");
 $stderr=array("pipe","w");
 $desc=array($stdin,$stdout,$stderr);
 $inputimage="./images/scan.jpg";
 $changeres=false;
 $changearea=false;
 $changeformat=false;
 $format="jpeg";
 $resolution=200;
 $t=0;
 $l=0;
 $dim=getimagesize($inputimage);
 $x=$dim[0];
 $y=$dim[1];
 $pages=1;
 $outputimage_format="";
 foreach(explode(" ",$options) as $option)
 {$matches=array();
  if(preg_match("/'--resolution=(.*)'/",$option,$matches))
  {$changeres=true;
   $resolution=$matches[1];
  }
  if(preg_match("/'--batch=(.*)'/",$option,$matches))
  {$outputimage_format=$matches[1];
  }
  if(preg_match("/'--batch-count=(.*)'/",$option,$matches))
  {$pages=intval($matches[1]);
  }
  if(preg_match("/'--format=(.*)'/",$option,$matches))
  {$changearea=true;
   $format=$matches[1];
  }
  if(preg_match("/'-([ltxy])=(.*)'/",$option,$matches))
  {if($matches[1]=='l')
   {$l=$matches[2];
   }
   elseif($matches[1]=='t')
   {$t=$matches[2];
   }
   elseif($matches[1]=='x')
   {$x=$matches[2];
   }
   elseif($matches[1]=='y')
   {$y=$matches[2];
   }
  }
 }
 if($outputimage_format=="")
 {echo "-l -t -x -y --format --resolution --batch --batch-count\n";
  exit();
 }
 if($changearea)
 {$tmpout=tname("crop.jpg");
  $command='convert -crop '.$width.'x'.$height.'+'.$x.'+'.$y.' '.$inputimage.' '.$tmpout;
  echo $command."\n";
  system($command);
  $inputimage=$tmpout;
 }
 if($changeres)
 {$tmpout=tname("res.jpg");
  $command='convert -units PixelsPerInch '.$inputimage.' -resample '.$resolution.' '.$tmpout;
  echo $command."\n";
  system($command);
  $inputimage=$tmpout;
 }
 if($changeformat)
 {$tmpout=tname('format.'.$format);
  $command='convert '.$inputimage.' '.$tmpout;
  echo $command."\n";
  system($command);
  $inputimage=$tmpout;
 }
 echo 'Pages: '.$pages."\n";
 for($page=0;$page<$pages;$page++)
 {$stdout='Scanning page '.($page+1)."\n";
  echo $stdout;
  file_put_contents($stdout_file,$stdout,FILE_APPEND);
  $outputimage=tname(str_replace("%d",$page,$outputimage_format));
  $fp_read=fopen($inputimage,"r");
  $fp_write=fopen($outputimage,"w");
  $filesize=filesize($inputimage);
  $read=0;
  while($read<$filesize)
  {$blocksize=round(1024000/$resolution);
   $readsize=0;
   if(($filesize-$read)>$blocksize)
   {$readsize=$blocksize;
   }
   else
   {$readsize=$filesize-$read;
   }
   $block=fread($fp_read,$readsize);
   $read+=$readsize;
   fwrite($fp_write,$block);
   $stdout='Progress: '.number_format(($read/$filesize)*100,1)."%\r";
   echo $stdout;
   file_put_contents($stdout_file,$stdout,FILE_APPEND);
   if(file_exists($term_file))
   {$signal=intval(file_get_content($term_file));
    proc_terminate($proc,2);
   }
   sleep(1);
  }
  fclose($fp_read);
  fclose($fp_write);
  $stdout='Scanned page '.($page+1).'. (scanner status = 5)'."\n";
  echo $stdout;
  file_put_contents($stdout_file,$stdout,FILE_APPEND);
 }
 file_put_contents($running_file,"false\n");
}
scanimage($_REQUEST['options']);
?>
