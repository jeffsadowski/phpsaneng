<?php
ignore_user_abort(true);
set_time_limit(0);

include 'remove.php';

function cpy($source, $dest){
    if(is_dir($source)) {
        $dir_handle=opendir($source);
        mkdir($dest);
        while($file=readdir($dir_handle)){
            if($file!="." && $file!=".."){
                if(is_dir($source."/".$file)){
                    if(!is_dir($dest."/".$file)){
                        mkdir($dest."/".$file);
                    }
                    cpy($source."/".$file, $dest."/".$file);
                } else {
                    copy($source."/".$file, $dest."/".$file);
                }
            }
        }
        closedir($dir_handle);
    } else {
        copy($source, $dest);
    }
}

function cc($s)
{$r='';
 foreach(str_split($s) as $c)
 {if(preg_match("/[a-z0-9-.]/i",$c))
  {$r.=$c;
  }
  else
  {$p='';
   if(ord($c)<16){$p='0';}
   $r.='_'.$p.dechex(ord($c));
 }}
 return $r;
}

if(!(isset($_REQUEST['tmpdir'])||isset($_REQUEST['testdir'])||isset($_REQUEST['sessiondir'])))
{if(count($argv)>1)
 {$_REQUEST['tmpdir']=$argv[1];
 }
 else
 {echo "You need to pass a tmpdir\n";
  exit();
}}

$DIR="";
if(isset($_REQUEST['sessiondir']))
{$DIR=session_save_path() . DIRECTORY_SEPARATOR . $_REQUEST['sessiondir'] . DIRECTORY_SEPARATOR;
}
elseif(isset($_REQUEST['tmpdir']))
{$DIR=session_save_path() . DIRECTORY_SEPARATOR . $_REQUEST['tmpdir'] . DIRECTORY_SEPARATOR;
}
elseif(isset($_REQUEST['testdir']))
{$DIR=$_REQUEST['testdir'] . DIRECTORY_SEPARATOR;
}

?>
