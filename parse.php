<?php
/*
filename:parse.php
written by:Jeff Sadowski
usage discussed:
	It takes an "options" argument pointing at
	a directory with stdout.txt text file inside
	expecting stdout.txt to be the output from
	scanimage -h -d '<DEVICE>'
	and outputs a json to be used by an html/javascript interface

example usage:
	http://192.168.1.27/parse_options.php?options=/tmp/scanimage-www-data-1505828853
*/

include 'cleanup.php';
$print=false;
$sections=array();
$arguments=array();
$section='default';
$sectionspace='';

if(file_exists($DIR."ran.txt"))
{$options=cc(substr(file_get_contents($DIR."ran.txt"),22,-11));
 cpy($DIR,session_save_path().DIRECTORY_SEPARATOR.'scanimage'.$options);
}

foreach(explode("\n",file_get_contents($DIR."stdout.txt").file_get_contents($DIR."stderr.txt")) as $line)
{$matches=array();
 if($print)
 {//echo $line."\n";
  //empty line
  if(preg_match("/^$/",$line))
  {if(count($arguments))
   {array_push($sections,array('arguments'=>$arguments,'section'=>$section));
    $arguments=array();
   }
   break;
  }
  elseif(preg_match("/^\s*-[^ ]/",$line))
  {//echo "argument\n";
   $argument=array();
   $argument['needsequals']=(!(strpos($line,'=')===false));
   $options="";
   if(preg_match("/^(-[^ ]*) .*,[.]{3}.* \[[^\]]*\]$/",trim($line)))
   {//echo "array\n";
    $argument['array']=true;
    $line=preg_replace("/,[.]{3}/","",$line);
   }

   //get units and remove it so it looks like a normal int to do the rest of the parsing
   if(preg_match("/^-[^ ]* [.0-9-][.0-9-]*[.]{2}[.0-9-][.0-9-]*([^.0-9- ][^.0-9- ]*) \(in steps of [.0-9-]*\) \[[^\]]*\]$/",trim($line),$matches))
   {$argument['units']=$matches[1];
    $line=str_replace($matches[1]." "," ",$line);
    $matches=array();
   }
   if(preg_match("/^-[^ ]* [.0-9-][.0-9-|]*([^.0-9- ][^.0-9- ]*) \[[^\]]*\]$/",trim($line),$matches))
   {$argument['units']=$matches[1];
    $line=str_replace($matches[1]." "," ",$line);
    $matches=array();
   }

   if(preg_match("/(-[^ ]*) <float> \[([^\]]*)\]/",trim($line),$matches))
   {$argument['type']='number';
    $argument['data-default']=$matches[2];
   }
   elseif(preg_match("/(-[^ ]*) <int> \[([^\]]*)\]/",trim($line),$matches))
   {$argument['type']='number';
    $argument['data-default']=$matches[2];
    $argument['step']='1';
   }
   elseif(preg_match("/(-[^ ]*) <string> \[([^\]]*)\]/",trim($line),$matches))
   {$argument['type']='string';
    $argument['data-default']=$matches[2];
   }
   elseif(preg_match("/(-[^ ]*) ([.0-9-]*)[.]{2}([.0-9-]*) \(in steps of ([^\)]*)\) \[([^\]]*)\]/",trim($line),$matches))
   {//echo "number\n\n";
    $argument['type']='number';
    $argument['min']=$matches[2];
    $argument['max']=$matches[3];
    $argument['step']=$matches[4];
    $argument['data-default']=$matches[5];
   }
   elseif(preg_match("/(-[^ ]*) (.*) \[([^\]]*)\]$/",trim($line),$matches))
   {//echo "select\n\n";
    $argument['type']='select';
    $options=$matches[2];
    $argument['data-default']=$matches[3];
   }
   elseif(preg_match("/(-[^\[]*)\[=\(([^\)]*)\)\] \[([^\]]*)\]$/",trim($line),$matches))
   {$argument['type']='select';
    $options=$matches[2];;
    $argument['data-default']=$matches[3];
   }
   elseif(preg_match("/(-[^ ]*) \[([^\]]*)\]$/",trim($line),$matches))
   {$argument['type']='flag';
   }
   else
   {
    $argument['type']='flag';
    $argument['name']=trim($line);
   }
   if(isset($argument['data-default'])&&$argument['data-default']=='inactive')
   {$argument['data-default']="";
   }
   if($options!="")
   {$options_with_labels=array();
    $option_label=array();
    foreach(explode('|',$options) as $option_with_label)
    {if(preg_match("/(.*)\[([^\]]*)\]/",$option_with_label,$option_label))
     {array_push($options_with_labels,array('option'=>$option_label[1],'label'=>$option_label[2]));
     }
     else
     {array_push($options_with_labels,array('option'=>$option_with_label));
    }}
    $argument['options']=$options_with_labels;
   }
   if(count($matches))
   {$extra='';
    if(isset($argument['type'])&&preg_match("/array/",$argument['type']))
    {$extra='[]';
    }
    $argument['name']=$matches[1].$extra;
   }
   if(count($argument))
   {//should add check for cookie to change the value via user input
    $argument['value']=$argument['data-default'];
    array_push($arguments,$argument);
  }}
  elseif(preg_match("/^(\s*)(.*)$/",$line,$matches))
  {if($sectionspace==''||$sectionspace==$matches[1])
   {//echo "section\n\n";
    if(count($arguments))
    {array_push($sections,array('arguments'=>$arguments,'section'=>$section));
     $arguments=array();
    }
    $sectionspace=$matches[1];
    $section=$matches[2];
   }
   else
   {//echo "comment\n\n";
    if(isset($arguments[count($arguments)-1]['label']))
    {$append=htmlspecialchars(trim($line));
     $join=" ";
     if(preg_match("/:/",$append))
     {$join="&#10;&#13;";
     }
     $arguments[count($arguments)-1]['label'].=$join.$append;
    }
    else
    {$arguments[count($arguments)-1]['label']=htmlspecialchars(trim($line));
 }}}}
 if(preg_match("/Options specific to device/",$line))
 {//echo "start\n";
  $print=true;
}}
header('Content-Type: application/json');
print json_encode($sections,JSON_PRETTY_PRINT);

chdir(session_save_path());
foreach (glob("scanimage-*") as $dirname)
{recursiveRemoveDirectory($dirname);
}


?>
