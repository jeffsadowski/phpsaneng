<!--
function to_date(string)
{
 var months3 = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
 var monthsF = ["January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
 var daysF = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
 var days3 = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
 var t=new Date();
 var Y=t.getFullYear();
 var m=t.getMonth()+1;
 var d=t.getDate();
 var a=t.getDay();
 var H=t.getHours();
 var M=t.getMinutes();
 var S=t.getSeconds();
 var z=t.getTimezoneOffset();
 var za=z
 var zs='-'
 if(z<0)
 {zs='+';
  za=0-z;
 }
 var zh=Math.floor(za/60);
 var zhpadz='';
 if(zh<10){zhpadz='0';}
 var zm=Math.floor(za-zh*60);
 if(zm<10){zmpadz='0';}
 var Z=zs+zhpadz+zh+zmpadz+zm
 var start=new Date(t.getFullYear(), 0, 0);
 var diff = t - start;
 var oneDay = 1000 * 60 * 60 * 24;
 var j = Math.floor(diff / oneDay);
 var mpads='';
 var mpadz='';
 var p='am';
 if(m<10){mpads=' ';mpadz='0';}
 var dpads='';
 var dpadz='';
 if(d<10)
 {dpads=' ';
  dpadz='0';
 }
 var Hpads='';
 var Hpadz='';
 if(H<10)
 {Hpads=' ';
  Hpadz='0';
 }
 I=H;
 if(I>12){I-=12;p='pm'}
 if(I==0){I=12;p='pm'}
 var Ipads='';
 var Ipadz='';
 if(I<10){Ipads=' ';Ipadz='0';}
 var Mpads='';
 var Mpadz='';
 if(M<10){Mpads=' ';Mpadz='0';}
 var Spads='';
 var Spadz='';
 if(S<10){Spads=' ';Spadz='0';}
 var jpads='';
 var jpadz='';
 if(j<100){jpads=' ';jpadz='0';}
 if(j<10){jpads='  ';jpadz='00';}

 //escape sequence
 string=string.replace(/%_/g,'%%_');
 string=string.replace(/%%/g,'%_');

 //common combos
 string=string.replace(/%F/g,'%Y-%m-%d');
 string=string.replace(/%D/g,'%m%d%y');
 string=string.replace(/%T/g,'%H-%M-%S');
 string=string.replace(/%R/g,'%H%M');

 //date like replacements
 string=string.replace(/%a/g,days3[a]);
 string=string.replace(/%A/g,daysF[a]);
 string=string.replace(/%b/g,months3[m]);
 string=string.replace(/%B/g,monthsF[m]);
 string=string.replace(/%C/g,(Y+'').substr(0,(Y+'').length-2));
 string=string.replace(/%d/g,dpadz+d);
 string=string.replace(/%e/g,dpads+d);
 string=string.replace(/%H/g,Hpadz+H);
 string=string.replace(/%I/g,Ipadz+I);
 string=string.replace(/%j/g,jpadz+j);
 string=string.replace(/%k/g,Hpads+H);
 string=string.replace(/%l/g,Ipads+I);
 string=string.replace(/%m/g,mpadz+m);
 string=string.replace(/%M/g,Mpadz+M);
 string=string.replace(/%p/g,p.toUpperCase());
 string=string.replace(/%P/g,p);
 string=string.replace(/%s/g,Math.floor(t/1000));
 string=string.replace(/%S/g,Spadz+S);
 string=string.replace(/%u/g,a);
 string=string.replace(/%y/g,(Y+'').substr(-2));
 string=string.replace(/%Y/g,(Y+''));
 string=string.replace(/%z/g,Z);
 string=string.replace(/%Z/g,z);

 //undo escape
 string=string.replace(/%_/g,'%');
 return string;
}
//-->
