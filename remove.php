<?php

function recursiveRemoveDirectory($directory)
{if(is_dir($directory))
 {foreach(glob($directory.DIRECTORY_SEPARATOR.'*') as $file)
  {if(is_dir($file))
   {recursiveRemoveDirectory($file);
   }
   else
   {unlink($file);
  }}
  rmdir($directory);
 }
 else
 {unlink($directory);
}}

?>
