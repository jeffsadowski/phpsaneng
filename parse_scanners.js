var _report_scanners;
var _report_options;
var refresh_char='&#8634;';

function refresh_scanners()
{query_scanners_attached(_report_scanners,_report_options,'&clear');
}

function query_scanners_attached(report_scanners,report_options,clear="")
{_report_scanners=report_scanners;
 _report_options=report_options;
 var xmlhttp = new XMLHttpRequest();
 var url = "scanimage.php?options='-L'&timestamp="+Date.now()+clear;
 var tmpdir="";
 var start=-1;
 var end=-1;
 report_scanners("Searching for scanners.");
 report_options("Wait for scanners to be found first.");
 xmlhttp.onreadystatechange = function()
 {if(this.readyState == 4 && this.status == 200)
  {if((start=this.responseText.search('sessiondir="'))!=-1)
   {end=this.responseText.substr(start+12).search('"');
    tmpdir=this.responseText.substr(start+12,end)
    scanner_query_running('sessiondir=',tmpdir,report_scanners,report_options);
   }
   else if((start=this.responseText.search('tmpdir="'))!=-1)
   {end=this.responseText.substr(start+8).search('"');
    tmpdir=this.responseText.substr(start+8,end)
    scanner_query_running('tmpdir=',tmpdir,report_scanners,report_options);
   }
   else
   {report_scanners("Something went wrong in query_scanners_attached.");
  }}
 };
 xmlhttp.open("GET", url, true);
 xmlhttp.send();
}

function scanner_query_running(type,tmpdir,report_scanners,report_options)
{var xmlhttp = new XMLHttpRequest();
 var url = "running_image.php?"+type+encodeURIComponent(tmpdir)+"&timestamp="+Date.now();
 var response="";
 var start=-1;
 var end=-1;
 xmlhttp.onreadystatechange = function()
 {if(this.readyState == 4 && this.status == 200)
  {if(this.responseText.search('running="')!=-1)
   {if(this.responseText.search('running="false"')!=-1)
    {which_scanners(type,tmpdir,report_scanners,report_options);
    }
    else
    {setTimeout(function(){scanner_query_running(type,tmpdir,report_scanners,report_options);},1000,tmpdir);
   }}
   else
   {report_scanners("Something went wrong in scanner_query_running.<br>type="+type+"<br>tmpdir="+tmpdir+"<br>response="+this.responseText);
 }}};
 xmlhttp.open("GET", url, true);
 xmlhttp.send();
}

function which_scanners(type,tmpdir,report_scanners,report_options)
{var xmlhttp = new XMLHttpRequest();
 var url = "scanners.php?"+type+encodeURIComponent(tmpdir)+'&cleanup';
 xmlhttp.onreadystatechange = function()
 {if(this.readyState == 4 && this.status == 200)
  {var myArr = JSON.parse(this.responseText);
   report_scanners(parse_scanners(myArr,report_options));
  }
 };
 xmlhttp.open("GET", url, true);
 xmlhttp.send();
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function parse_scanners(json,report_options)
{var html='Scanners:<br><select id="-d" name="-d" onchange="fetch_scanner_options(this.options[this.selectedIndex].value,'+report_options.name+')">';
 var first_device=true;
 var scanner=decodeURIComponent(getCookie("scanner"));
 var selected='';
 for(i in json)
 {selected='';
  if(json[i]['device']==scanner)
  {selected=' selected="selected"'
  }
  html+='<option value="'+json[i]['device']+'"'+selected+'>'+json[i]['name']+'</option>';
  if(first_device)
  {if(scanner=="")
   {scanner=json[i]['device'];
   }
   fetch_scanner_options(scanner,report_options);
   first_device=false;
 }}
 html+='</select> <input type="button" onclick="refresh_scanners()" value="'+refresh_char+'">';
 return html;
}

function fetch_scanner_options(scanner,report_function)
{var xmlhttp = new XMLHttpRequest();
 var url = "scanimage.php?options='-h'+'-d'+'"+encodeURI(scanner)+"'";
 var response="";
 var start=-1;
 var end=-1;
 document.cookie = "scanner="+encodeURIComponent(scanner);
 report_function("Retreaving scanner options.");
 xmlhttp.onreadystatechange = function()
 {if(this.readyState == 4 && this.status == 200)
  {
   if((start=this.responseText.search('sessiondir="'))!=-1)
   {end=this.responseText.substr(start+12).search('"');
    tmpdir=this.responseText.substr(start+12,end)
    options_query_running('sessiondir=',tmpdir,report_function);
   }
   else if((start=this.responseText.search('tmpdir="'))!=-1)
   {end=this.responseText.substr(start+8).search('"');
    tmpdir=this.responseText.substr(start+8,end)
    options_query_running('tmpdir=',tmpdir,report_function);
   }
   else
   {report_options('Error in fetch_scanner_options. '+this.responseText);
  }}
 };
 xmlhttp.open("GET", url, true);
 xmlhttp.send();
}


function options_query_running(type,tmpdir,report_function)
{var xmlhttp = new XMLHttpRequest();
 var url = "running_image.php?"+type+encodeURIComponent(tmpdir);
 var response="";
 var start=-1;
 var end=-1;
 xmlhttp.onreadystatechange = function()
 {if(this.readyState == 4 && this.status == 200)
  {if(this.responseText.search('running="')!=-1)
   {if(this.responseText.search('running="false"')!=-1)
    {scanner_options('sessiondir=',tmpdir,report_function);
    }
    else
    {setTimeout(function(){options_query_running(type,tmpdir,report_function);},1000,tmpdir);
   }}
   else
   {report_function("Error in options_query_running. "+this.responseText);
 }}};
 xmlhttp.open("GET", url, true);
 xmlhttp.send();
}

function scanner_options(type,tmpdir,report_function)
{var xmlhttp = new XMLHttpRequest();
 var url = "parse.php?"+type+tmpdir;
 xmlhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
        var myArr = JSON.parse(this.responseText);
        report_function(parse(myArr));
    }
 };
 xmlhttp.open("GET", url, true);
 xmlhttp.send();
}

function array(name)
{var parts=name.split(' ');
 var countobj=document.getElementById('--count '+parts[1]);
 var count=countobj.value;
 if(parts[0]=='--add')
 {countobj.value++;
  var values={};
  for(var x=0;x<count;x++)
  {var item=document.getElementById('--'+x+' '+parts[1]);
   values['--'+x+' '+parts[1]]=item.value;
  }
  var orgobj=document.getElementById('--original '+parts[1]);
  var objstr=orgobj.innerHTML;
  var copy=objstr.replace('id="'+parts[1],'id="--'+count+' '+parts[1]);
  var divobj=document.getElementById('--div '+parts[1]);
  divobj.innerHTML+=copy;
  for(var x=0;x<count;x++)
  {var item=document.getElementById('--'+x+' '+parts[1]);
   item.value=values['--'+x+' '+parts[1]];
 }}
 else
 {if(count>0)
  {countobj.value--;
   count--;
   var node=document.getElementById('--'+count+' '+parts[1]);
   node.parentNode.removeChild(node);
}}}

function array_creator(name,disabled)
{var html='<'+'div style="display: inline" id="--div '+name+'">'+'<'+'/div>';
 html+='<'+'input type="hidden" id="--count '+name+'" value=0>';
 html+='<'+'input id="--add '+name+'" type="button" value="+" onclick="array(this.id)"'+disabled+'>';
 html+='<'+'input id="--del '+name+'" type="button" value="-" onclick=array(this.id)'+disabled+'>';
 return html;
}

function digits_only(object)
{object.value=object.value.replace(/[^0-9]/g,'');
}

function enable_disable(checkbox,objectid)
{var obj=document.getElementById(objectid);
 var countobj=document.getElementById('--count '+objectid);
 var addobj=document.getElementById('--add '+objectid);
 var delobj=document.getElementById('--del '+objectid);
 if(checkbox.checked)
 {obj.disabled=false;
  if(countobj)
  {addobj.disabled=false;
   delobj.disabled=false;
   for(var x=0;x<countobj.value;x++)
   {var item=document.getElementById('--'+x+' '+objectid);
    item.disabled=false;
 }}}
 else
 {obj.disabled=true
  if(countobj)
  {addobj.disabled=true;
   delobj.disabled=true;
   for(var x=0;x<countobj.value;x++)
   {var item=document.getElementById('--'+x+' '+objectid);
    item.disabled=true;
}}}}

function option_reset(objectid,default_value)
{var obj=document.getElementById(objectid);
 var countobj=document.getElementById('--count '+objectid);
 obj.value=default_value;
 if(countobj)
 {while(countobj.value>0)
  {array('--del '+objectid);
}}}

function wrap(object,property)
{if(object.hasOwnProperty(property))
 {return ' '+property+'="'+object[property]+'"';
 }
 else
 {return '';
}}

function parse(json)
{var html='';
 for(i in json)
 {html+=json[i]['section']+'<'+'table>'+"\n";
  for(j in json[i]['arguments'])
  {html+='<'+'tr>'+'<'+'td>';
   var argument=json[i]['arguments'][j];
   var name=argument['name'];
   if(json[i]['arguments'][j]['needsequals'])
   {name=name+'=';
   }
   var id=' id="'+name+'" name="'+name+'"';
   var disabled=' disabled="disabled"';
   if(!argument.hasOwnProperty('label'))
   {argument['label']=name;
   }
   var units=wrap(argument,'units');
   html+='<'+'input id="'+name+' checkbox" type="checkbox" onclick="enable_disable(this,'+"'"+name+"'"+')">';
   html+='<'+'label>'+'<'+'span title="'+argument['label']+'">'+name+units+'<'+'/span>'+'<'+'/label>'+"\n";
   html+='<'+'div id="--original '+name+'" style="display: inline">';
   var valuetext=wrap(argument,'value');
   var value='';
   if(valuetext!='')
   {value=argument['data-default'];
   }
   if(argument['type']=='string')
   {
    html+='<'+'input name="'+name+'" value="'+value+'"'+id+disabled+'>'+"\n";
   }
   if(argument['type']=='flag')
   {
    html+='<'+'input name="'+name+'" type="hidden" value=""'+id+disabled+'>'+"\n";
   }
   if(argument['type']=='number')
   {var mintext=wrap(argument,'min');
    var maxtext=wrap(argument,'max');
    var steptext=wrap(argument,'step');
    var defaulttext=wrap(argument,'data-default');
    html+='<'+'span title="'+(mintext+maxtext+steptext+defaulttext).replace(/"/g,"'")+'">'+'<'+'input type="number"'+valuetext+id+mintext+maxtext+steptext+defaulttext+disabled+'><'+'/span>'+"\n";
   }
   if(argument['type']=='select')
   {html+='<'+'select '+id+disabled+'>'+"\n";
    for(k in argument['options'])
    {var mydefault='';
     var option=argument['options'][k]['option'];
     if(value==option)
     {mydefault=' selected'
     }
     if(argument['options'][k].hasOwnProperty('label'))
     {label='['+argument['options'][k]['label']+']';
     }
     else
     {label='';
     }
     html+='<'+'option value="'+option+'"'+mydefault+'>'+option+label+'</option>'+"\n";
    }
    html+='<'+'/select>'+"\n";
   }
   html+='<'+'/div>';
   if(argument.hasOwnProperty('array'))
   {html+=array_creator(name,disabled);
   }
   if(argument['type']!='flag')
   {html+='<'+'input id="--reset '+name+'" type=button value="'+refresh_char+'" onclick="option_reset('+"'"+name+"'"+','+"'"+value+"'"+')">';
   }
   html+='<'+'/tr></td>'+"\n";
  }
  html+='<'+'/table>'+"\n";
 }
 return html;
}

function full_scan(options,report_function)
{var xmlhttp = new XMLHttpRequest();
 var url = "scanimage.php?options="+options+"'";
 var response="";
 var start=-1;
 var end=-1;
 report_function("Retreaving scanner options.");
 xmlhttp.onreadystatechange = function()
 {if(this.readyState == 4 && this.status == 200)
  {
   if((start=this.responseText.search('sessiondir="'))!=-1)
   {end=this.responseText.substr(start+12).search('"');
    tmpdir=this.responseText.substr(start+12,end)
    full_scan_running('sessiondir=',tmpdir,report_function);
   }
   else if((start=this.responseText.search('tmpdir="'))!=-1)
   {end=this.responseText.substr(start+8).search('"');
    tmpdir=this.responseText.substr(start+8,end)
    full_scan_running('tmpdir=',tmpdir,report_function);
   }
   else
   {report_options('Error in full_scan. '+this.responseText);
  }}
 };
 xmlhttp.open("GET", url, true);
 xmlhttp.send();
}


function full_scan_running(type,tmpdir,report_function)
{var xmlhttp = new XMLHttpRequest();
 var url = "running_image.php?"+type+encodeURIComponent(tmpdir);
 xmlhttp.onreadystatechange = function()
 {if(this.readyState == 4 && this.status == 200)
  {if(this.responseText.search('running="')!=-1)
   {if(this.responseText.search('running="false"')!=-1)
    {report_function(this.responseText);
    }
    else
    {report_function(this.responseText);
     setTimeout(function(){full_scan_running(type,tmpdir,report_function);},1000,tmpdir);
   }}
   else
   {report_function("Error in full_scan_running. "+this.responseText);
 }}};
 xmlhttp.open("GET", url, true);
 xmlhttp.send();
}
