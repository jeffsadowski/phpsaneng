<?php
function date_replace($string)
{$timestamp = time();
 $patterns=array();
 if(preg_match_all('/[\\\](.)/',$string,$patterns))
 {array_shift($patterns);
  foreach($patterns as $matches)
  {foreach(array_unique($matches) as $match)
   {$pattern='/[\\\]'.$match.'/';
    $string=preg_replace($pattern,date($match,$timestamp),$string);
 }}}
 return $string;
}

$string="\Y-\m-\d \H:\i:\s";
echo date_replace($string)."\n";
$string="\p";
echo date_replace($string)."\n";

?>
