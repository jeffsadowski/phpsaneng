<?php
include 'cleanup.php';

chdir(session_save_path());
cpy($DIR,'scanimage_27-L_27');


$scanners=array();
foreach(explode("\n",file_get_contents($DIR.'stdout.txt').file_get_contents($DIR.'stderr.txt')) as $line)
{$matches=array();
 $scanner=array();
 if(preg_match("/^device [^ ]([^ ]*)[^ ] is a *(.*)$/",$line,$matches))
 {$scanner['device']=$matches[1];
  $scanner['name']=$matches[2];
  array_push($scanners,$scanner);
 }
}
header('Content-Type: application/json');
print json_encode($scanners,JSON_PRETTY_PRINT);

/*
if(isset($_REQUEST['sessiondir']))
{chdir(session_save_path());
 sleep(1);
 recursiveRemoveDirectory($_REQUEST['sessiondir']);
}
*/
?>
