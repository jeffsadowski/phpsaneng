<?php
/*

  http://raspberrypi/scanimage.php?options=-h+-d=brother2:bus1;dev1
  I use urlencode for options

  commandline
  php scanimage.php -h -d "brother2:bus1;dev1"

  above should start another php process in the background via curl

*/

ignore_user_abort(true);
set_time_limit(0);

include 'working.php';

echo 'Working="'.$WORKING.'" <br>';

function cc($s)
{$r='';
 foreach(str_split($s) as $c)
 {if(preg_match("/[a-z0-9-.]/i",$c))
  {$r.=$c;
  }
  else
  {$p='';
   if(ord($c)<16){$p='0';}
   $r.='_'.$p.dechex(ord($c));
 }}
 return $r;
}

if(sizeof($argv)>0)
{$myargv=$argv;
 array_shift($myargv);
 if(sizeof($argv)>1)
 {$_REQUEST['options']="'".implode("' '",$myargv)."'";
 }
 $usertime='scanimage-'.posix_geteuid().'-'.time();
 $_REQUEST['tmpdir']='/tmp/'. $usertime;
 $_REQUEST['background']=true;
 if(!mkdir($_REQUEST['tmpdir']))
 {echo "Can not create directory '".$_REQUEST['tmpdir']."'\n";
  exit();
}}

function clean($string)
{$string = str_replace('`', '', $string);
 $string = str_replace('$', '', $string);
 $string = str_replace('(', '', $string);
 $string = str_replace(')', '', $string);
 $string = str_replace('*', '', $string);
 $string = str_replace('{', '', $string);
 $string = str_replace('}', '', $string);
 $string = str_replace('[', '', $string);
 $string = str_replace(']', '', $string);
 return $string;
}

if(isset($_REQUEST['options']))
{$_REQUEST['options']=clean($_REQUEST['options']);
}

$frontend=false;
if(isset($_REQUEST['background']))
{//run only output to files
 echo "Running in background or testing\n";
 $DIR=session_save_path() . DIRECTORY_SEPARATOR .
      $_REQUEST['tmpdir'] . DIRECTORY_SEPARATOR;
 echo $DIR."\n<br>\n";
 $ran_file=$DIR.'ran.txt';
 $stdout_file=$DIR.'stdout.txt';
 $stderr_file=$DIR.'stderr.txt';
 $running_file=$DIR.'running.txt';
 $term_file=$DIR.'term.txt';
 // fix escape vector
 if(!isset($_REQUEST['options'])){$_REQUEST['options']="-L";}
 $options=explode("'",$_REQUEST['options']);
 $_REQUEST['options']="";
 foreach($options as $option)
 {if(!preg_match("/^\s*$/",$option))
  {$_REQUEST['options'].=" '".$option."'";
 }}
 echo $_REQUEST['options'];
}
else
{if(!function_exists("curl_init"))
 {echo 'I need curl_init to work.';
  exit();
 }
 if(isset($_REQUEST['options'])&&!isset($_REQUEST['clear']))
 {echo 'options="'.$_REQUEST['options'].'" <br>';
  if(!(
       (strpos($_REQUEST['options'],"-L")===false) &&
       (strpos($_REQUEST['options'],"'-L'")===false) &&
       (strpos($_REQUEST['options'],"-h")===false) &&
       (strpos($_REQUEST['options'],"'-h'")===false)
  ))
  {$tmpdir='scanimage'.cc($_REQUEST['options']);
   $DIR=session_save_path() . DIRECTORY_SEPARATOR . $tmpdir . DIRECTORY_SEPARATOR;
   if(file_exists($DIR.'stdout.txt')&&file_exists($DIR.'running.txt'))
   {echo 'tmpdir="'.$tmpdir.'"<br>'."\n";
    exit();
 }}}
 if(!isset($_REQUEST['tmpdir']))
 {$_REQUEST['tmpdir']='scanimage-'.posix_geteuid().'-'.time();
  $DIR=session_save_path() . DIRECTORY_SEPARATOR . $_REQUEST['tmpdir'];
  if(!mkdir($DIR))
  {echo "ERROR Could not create a temp file\n";
   exit();
  }
  echo 'sessiondir="'.$_REQUEST['tmpdir'].'"<br>'."\n";
 }

 $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
 $join='?';
 if(preg_match("/[?]/",$actual_link))
 {$join='&';
 }
 $url=$actual_link.$join.'session='.session_id().'&background=true'.$BYIP.'&tmpdir='.urlencode($_REQUEST['tmpdir']);
 $ch = curl_init();
 curl_setopt($ch, CURLOPT_URL, $url);
 curl_setopt($ch, CURLOPT_TIMEOUT_MS, 1000);
 curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
 curl_exec($ch);
 curl_close($ch);
 exit();
}

//example
//scanimage('-h -d "brother2:bus1;dev1"');

function scanimage($options)
{global $stdout_file,$stderr_file,$running_file,$ran_file,$term_file;
 file_put_contents($running_file,"true\n");
 $stdin=array("pipe","r");
 $stdout=array("pipe","w");
 $stderr=array("pipe","w");
 $desc=array($stdin,$stdout,$stderr);
 $command='script -c "scanimage '.$options.'" /dev/null';
 file_put_contents($ran_file,$command);
 $proc=proc_open($command,$desc,$pipes);
 stream_set_blocking($pipes[1],false);
 stream_set_blocking($pipes[2],false);
 $stdout_appended="";
 $stderr_appended="";
 $old_stdout_appended="x";
 $old_stderr_appended="x";
 for(;;)
 {$stat=proc_get_status($proc);
  $mystdout=fgets($pipes[1]);
  $mystderr=fgets($pipes[2]);
  if($mystdout=="" && $mystderr=="")
  {//echo "sleeping\n";
   if($old_stdout_appended!=$stdout_appended)
   {file_put_contents($stdout_file,$stdout_appended);
    $old_stdout_appended=$stdout_appended;
   }
   if($old_stderr_appended!=$stderr_appended)
   {file_put_contents($stderr_file,$stderr_appended);
    $old_stderr_appended=$stderr_appended;
   }
   sleep(1);
   if($stat['running']!=1)
   {break;
  }}
  else
  {echo $mystdout;
   $stdout_appended.=$mystdout;
   $stderr_appended.=$mystderr;
  }
  if(file_exists($term_file))
  {$signal=intval(file_get_content($term_file));
   proc_terminate($proc,2);
 }}
 fclose($pipes[1]);
 fclose($pipes[2]);
 file_put_contents($running_file,"false\n");
}
scanimage($_REQUEST['options']);
?>
